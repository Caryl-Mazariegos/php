 <!DOCTYPE HTML>

<html>
	<head>
		<title>SQL</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />

		<?php 
				//Conexión a la base de datos 
				$servidor = "localhost"; //Nombre del servidor 
				$usuario = "User"; //Nombre de usuario en tu servidor 
				$password = "user"; //Contraseña del usuario 
				$base = "user"; //Nombre de la BD 
				$conexion = mysqli_connect($servidor, $usuario, $password) or die("Error al conectarse al servidor"); 
				mysqli_select_db($conexion, $base) or die("Error al conectarse a la base de datos"); 

					$buscar = mysqli_query($conexion, "SELECT * FROM leccion WHERE idleccion = 140"); 
						if (mysqli_num_rows($buscar) > 0) { 
		?> 

	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
		<header id="header">
					<h1 id="logo"><a href="kinal.php">Kinal Academy</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="kinal.php">Inicio</a></li>
							<li>
								<a href="#">Cursos</a>
								<ul>
									<li><a href="java.php">Java</a></li>
									<li><a href="html.php">HTML</a></li>
									<li><a href="php.php">Php</a></li>
									<li><a href="c.php">C#</a></li>
									<li><a href="sql.php">SQL Server</a></li>
									<li><a href="android.php">Android</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</header>

				<?php 
						while ($datos = mysqli_fetch_array($buscar)){ 
				?> 

				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2><?=$datos["nombre"]?> </h2>
						</header>

						<!-- Content -->
							<section id="content">
							<div style="text-align:center;">
								<iframe width="854" style="text-align:center" height="510" src=<?=$datos["video"]?> controls></iframe>
							</div>
								<h3>Descripcion</h3>
								<p><?=$datos["descripcion"]?></p>
								
							</section>

							<ul class="actions" style="text-align:center;">
								<li><a href="sleccion12.php" class="button">Siguiente Leccion</a></li>
							</ul>
	

					</div>
				</div>

			<!-- Footer -->
		
				<footer id="footer">
					<ul class="copyright">
						<li>&copy; Centro Educativo Técnico Laboral Kinal.</li><li>Caryl Mazariegos</li>
					</ul>
				</footer>


		</div>

		<?php 
		} 
		mysqli_free_result($buscar); 
		?> 

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

			<?php 
			} else { 
			echo "No se encontraron datos en la base de datos"; 
			} 
			?> 

	</body>
</html>
