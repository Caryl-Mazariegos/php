-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-07-2015 a las 15:48:13
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `user`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leccion`
--

CREATE TABLE IF NOT EXISTS `leccion` (
  `idleccion` int(11) NOT NULL,
  `nombre` varchar(128) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `video` varchar(128) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(550) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `leccion`
--

INSERT INTO `leccion` (`idleccion`, `nombre`, `video`, `descripcion`) VALUES
(1, 'Empezando el Curso de Java ', 'https://www.youtube.com/embed/QgSeDCmB-qQ?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Primer video del Curso de Java en el que se explica en que va a consistir y se explica como instalar los programas necesarios pa'),
(2, 'Primer Hola Mundo', 'https://www.youtube.com/embed/hiAb7Jld1RI?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Se enseña ha realizar el primer Hola Mundo en Java, ademas del manejo basico de Eclipse y los metodos System.out.print y System.'),
(3, 'Atributos', 'https://www.youtube.com/embed/m_qLXDyJRr4?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Muestra de que son y para que sirven los atributos en Java ademas de como mostrarlos por pantalla.'),
(4, 'Operaciones con Atributos', 'https://www.youtube.com/embed/m_qLXDyJRr4?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Video en el que se explica que son las operaciones con los atributos y como se realizan estas. La base de la programacion!!'),
(5, 'Introduciendo Datos por Teclado', 'https://www.youtube.com/embed/MDtkR55Agcc?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Explicacion de como introducir datos por teclado con la clase Scanner.'),
(6, ' Ejemplo Intercambio Atributos', 'https://www.youtube.com/embed/Tj9Jj4jUBuY?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Ejemplo en el que se aplican conceptos ya explicados y en el que se realiza el intercambio de valores de dos atributos.'),
(7, 'if/else y Ejemplo numero par', 'https://www.youtube.com/embed/-HC6KTKr138?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Explicacion de que son las Sentencias Selectivas y ejemplo con if/else y comprobar si un numero es par o impar.'),
(8, 'Tipo Boolean y Condicionales Anidados', 'https://www.youtube.com/embed/zLJlXFkFMgQ?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Se explica que son los condicionales anidados con un ejemplo sobre temperaturas y tras esto se explica que es el tipo booleano.'),
(9, 'Tipo Char y Switch', 'https://www.youtube.com/embed/4fYgrMMcGhQ?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Explicacion de que son las variables de tipo char y que es y como se utiliza la sentencia selectiva switch.'),
(10, 'Condiciones Multiples', 'https://www.youtube.com/embed/-CbWd84XZZY?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'En este video se explica que son las condiciones multiples con un ejercicio en el que se explica como introducir una hora correcta y tras sumarle un segundo, que se incremente.'),
(11, ' Bucles While y For', 'https://www.youtube.com/embed/xhc4gxg26NE?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Introducción a que es y como se utilizan los bucles For y While en Java.'),
(12, ' Ejemplo Bucles For y While', 'https://www.youtube.com/embed/nM4RppH5SI4?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Explicacion de dos ejemplos usando los bucles for y while para asentar los conocimientos del video anterior.'),
(13, 'Bucle Do While', 'https://www.youtube.com/embed/mD4mfWecMGE?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Video donde se explica que es el bucle Do While y como usarlo junto con las diferencias que tiene con el resto de bucles.'),
(14, 'Ejemplo Menu Bucle Do While', 'https://www.youtube.com/embed/dVCTDygWFlE?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Ejemplo en el que vemos como realizar un Menu con un bucle Do While y un Switch.'),
(15, 'Bucles Anidados', 'https://www.youtube.com/embed/aG52nbPsb0Y?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Ejemplo en el que se realiza un programa para hacer las tablas de multiplicar del 1 al 10.'),
(16, 'Aprender a Depurar y Ejemplo de Numeros Primos', 'https://www.youtube.com/embed/R28fiMsiX2k?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Video dividido en dos partes, por un lado se enseña a utilizar la herramienta de nuestro IDE Eclipse para depurar y en la segunda parte, se enseña a hacer un ejercicio en el que al final planteare algunas dudas y el uso del depurador.\r\n'),
(17, 'Condiciones Multiples y eficiencia', 'https://www.youtube.com/embed/yK1n02Nc_EQ?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Video en el que se explica como conseguir todos los numeros primos entre dos numeros, se hace un repaso en el uso del depurador y se explica como poner una condicion multiple ayuda en la eficiencia.'),
(18, 'Que es un Vector?', 'https://www.youtube.com/embed/7BFICVA5F_s?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'En este video se explica que es, como se declara y utiliza un vector. Para esto se realiza un ejemplo sencillo en el que se escribe y luego se lee de un vector.'),
(19, 'Nueva declaracion de Vectores Y Ejemplo', 'https://www.youtube.com/embed/D7TrueDqyUM?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Se explica un nuevo tipo de declaracion de vectores para hacerlo en tiempo de ejecucion y un ejemplo donde se aprende a realizar la media de todos los elementos de un vector.'),
(20, 'Length y Ordenamiento de un Vector', 'https://www.youtube.com/embed/A92SkWoou-s?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Explicacion de una nueva forma para declarar vectores ya inicializados y el metodo length para saber cual es la longitud de un vector. Todo esto se explica con un ejemplo en el que se ve como ordenar un vector de menor a mayor mediante el metodo de la burbuja.'),
(21, 'Comparacion de Vectores y Direccion de Memoria.', 'https://www.youtube.com/embed/nREfQf4yVPE?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'En este video se explica como comparar dos vectores y que son las direccion es de memoria.\r\n\r\nTambien se explica que ocurre cuando te sales en un vector del rango de tu longitud.'),
(22, 'Que son los String y Como se comparan?', 'https://www.youtube.com/embed/cyE3yOBY7r8?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Introduccion sobre que son los String y y como utilizarlos y compararlos'),
(23, 'Scanner y Cifrado de Cesar', 'https://www.youtube.com/embed/KGTO4Wh-rOY?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'En el video se explica como introducir por teclado Strings y se hace un ejemplo en el que se realiza el cifrado de cesar que sirve para aprender a acceder a cada una de las letras de un String.'),
(24, 'Que es una matriz?', 'https://www.youtube.com/embed/Pk3vBlzQEdM?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Explicacion de que es una matriz y como escribir y acceder a ella.'),
(25, 'Comparacion de Matrices y Autoincremento', 'https://www.youtube.com/embed/fJkZ_2CtsLo?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Se enseña a declarar de una manera diferente las matrices, como se comparan dos matrices y como funciona realmente el autoincremento.'),
(26, ' Introduccion a la Programacion Orientada a Objetos', 'https://www.youtube.com/embed/UvBElXtcHFM?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Primer video donde se expica hace una introduccion a la Programacion Orientada a Objetos (POO), en el se explica que es una clase y un objeto.'),
(27, 'Public y Private en POO', 'https://www.youtube.com/embed/kI3fSNWVNuU?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Video que hace un breve repaso de la POO y donde se explica que son private y public.'),
(28, 'Que son los Metodos?', 'https://www.youtube.com/embed/9-Aqdxv1BPU?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Se explica que son y como utilizar los metodos de una clase.'),
(29, 'Ejemplo POO con Batalla por turnos', 'https://www.youtube.com/embed/lpIK4og-sr0?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Video en el que se resume y ejercita lo visto hasta el momento de programación orientada a objetos haciendo un pequeño sistema de batalla por turnos muy simple.'),
(30, 'Que es un Constructor?', 'https://www.youtube.com/embed/UjgqB5WAWPA?list=PLw8RQJQ8K1yQDqPyDRzt-h8YlBj96OwMP', 'Video en el que se explica que son los Constructores en la Programacion Orientada a Objetos (POO).'),
(31, ' Introducción', 'https://www.youtube.com/embed/sS3oDIcHNFo?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Les presentamos el primer video de nuestra nueva serie de video tutoriales para aprender a programar en Android desde cero!'),
(32, 'Configurar PC para desarrollar para Android', 'https://www.youtube.com/embed/q5ngUJXn9pw?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicación completa sobre como configurar nuestros ordenadores para poder empezar a programar en android.'),
(33, '"Hola Mundo"', 'https://www.youtube.com/embed/urujGz3esYE?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'En este video tutorial veremos como crear y ejecutar nuestro primer programa de "Hola Mundo" para Android.'),
(34, 'Secciones de Eclipse y estructura de un proyecto en Android', 'https://www.youtube.com/embed/kjNIUkDUvzY?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicación sobre como es la estructura de android.'),
(35, 'AndroidManifest.xml', 'https://www.youtube.com/embed/oNbXiFyyPhs?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicación sobre este importante componente de nuestro IDE.'),
(36, 'Tipos de datos básicos', 'https://www.youtube.com/embed/gENpcdRZyjE?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion tipos de datos en nuestra aplicacion'),
(37, 'Estructuras de control básicas en Java', 'https://www.youtube.com/embed/hlTFwuYlk78?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Estructura sobre los controles básicos haciendo uso de Java.'),
(38, ' Diseño de Layouts', 'https://www.youtube.com/embed/YKXXLC-vNwI?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como diseñar nuestros layouts para las vistas de nuestra aplicacion.'),
(39, 'TextView', 'https://www.youtube.com/embed/M-8WbMbu8k8?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre esta herramienta de android para mostrar texto en nuestra aplicacion'),
(40, 'EditText', 'https://www.youtube.com/embed/QRimQ7r7kuk?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre esta herramienta de android que nos permite ingresar texto en nuestra aplicacion'),
(41, 'Button', 'https://www.youtube.com/embed/RYQFzBai1_k?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGP', 'Explicacion sobre como agregar un boton a nuestra aplicacion android.'),
(42, 'Event OnClickListener', 'https://www.youtube.com/embed/2QKt8b-T_SI?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como realizar actividades al momento de hacer click en nuestra aplicacion android'),
(43, 'Evento OnLongClickListener', 'https://www.youtube.com/embed/1x7sHH2ojxw?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Profundizamos sobre las actividades que realiza ClickListener.'),
(44, 'Event OnKeyListener', 'https://www.youtube.com/embed/DnB6Sf13O-E?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Profundizamos sobre las actividades que realiza ClickListener.'),
(45, 'Eventos OnTouchListener', 'https://www.youtube.com/embed/rlf8Pv1DzYg?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Profundizamos sobre las actividades que realiza TouchListener.'),
(46, 'Crear un archivo .XML', 'https://www.youtube.com/embed/SDXZMSo7tOU?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Como crear un archivo .xml y agregarle funcionalidades para nuestra aplicacion android.'),
(47, 'Control de mensajes', 'https://www.youtube.com/embed/6xAH9HxRawg?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', ''),
(48, 'Crear una nueva Activity', 'https://www.youtube.com/embed/1FdUjKipgFc?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como crear una nueva actividad en nuestra aplicacion android.'),
(49, 'Intents (continuación de Activity)\r\n', 'https://www.youtube.com/embed/FXVQuRe_lXc?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Continua la explicación cobre las vistas.'),
(50, 'Objeto bundle (pasar datos entre activity)', 'https://www.youtube.com/embed/bu-HnNbBhCY?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como transportar datos entre vistas.'),
(51, 'Gestión de Imágenes', 'https://www.youtube.com/embed/0mldZFd3BPY?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion de como agregar imagenes a nuestra aplicacion android.'),
(52, 'ToggleButton', 'https://www.youtube.com/embed/o0sh1RSRj_4?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', ''),
(53, 'RadioGroup & RadioButton', 'https://www.youtube.com/embed/INaqbqif8TE?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion de como agregar estos diferenets tipos de botones.'),
(54, 'ListView', 'https://www.youtube.com/embed/MOwGgU7Weag?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como agregar y mostrar listas en nuestra aplicacion.'),
(55, 'Spinner', 'https://www.youtube.com/embed/_i4cFwWHreI?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como agregar y mostrar spinners en nuestra aplicacion.'),
(56, 'ViewFlipper', 'https://www.youtube.com/embed/_i4cFwWHreI?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion de ViewFlipper que nos permite fácilmente paginar contenido dentro de los XML'),
(57, 'TabHost', 'https://www.youtube.com/embed/ONIsowneJVk?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como agregar tabs en nuestra aplicación.'),
(58, 'Tiempos en Android', 'https://www.youtube.com/embed/dqfCTX0Q85Q?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', ''),
(59, 'Personalizar Botones', 'https://www.youtube.com/embed/bClGAEY_DQ8?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicación sobre como modificar nuestros botones.'),
(60, 'Gestión de sonidos MediaPlayer', 'https://www.youtube.com/embed/Gfm_nDm6Ues?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como agregar sonidos a nuestra aplicacion android por medio de media player.'),
(61, 'Gestión de sonidos SoundPool', 'https://www.youtube.com/embed/uE4DWNbmvko?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como agregar sonidos a nuestra aplicacion android por medio de sound pool.'),
(62, 'Introduccion a SQL Bases de datos', 'https://www.youtube.com/embed/w0VDjh3DsPE?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como conectar bases de datos a nuestra aplicacion.'),
(63, 'Crear Base de Datos con SQLite', 'https://www.youtube.com/embed/fNLTz_MtqsE?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como crear bases de datos a nuestra aplicacion.'),
(64, 'Abrir, cerrar e ingresar a la Base de Datos', 'https://www.youtube.com/embed/UTFqOAgAHyg?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como manipular bases de datos a nuestra aplicacion.'),
(65, 'Mostrar datos', 'https://www.youtube.com/embed/PR2rzdfx7Rs?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como mosrtar nuestros datos de bases de datos a nuestra aplicacion.'),
(66, 'Diseño de app final Naweb', 'https://www.youtube.com/embed/GnUD_qwIR5c?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Explicacion sobre como queda nuestro diseño de nuestra app.'),
(67, 'App final Naweb', 'https://www.youtube.com/embed/MvOFZq985U0?list=PLpOqH6AE0tNjZdEeWgHvxkXoeoPCvDGPL', 'Muestra app final.'),
(68, 'Que es HTML?', 'https://www.youtube.com/embed/uymhWqUyjfc?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p\r\nhttps://www.youtube.com/embed/MvOFZq985U0?lis', 'Que es HTML? Para que sirve? En que se basa? Que requerimos para programar en HTML? Como visualizar nuestras paginas web?'),
(69, 'Que es HTML + comandos basicos"', 'https://www.youtube.com/embed/cPVE43YbdJ0?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Explicacion sobre comandos basicos de html'),
(70, 'Etiquetas basicas', 'https://www.youtube.com/embed/gHIQ1ik1x2U?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Estructura del Lenguaje de HTML. Etiquetas basicas HTML. Etiqueta HTML. Etiqueta HEAD. Etiqueta BODY. Etiqueta STRONG. Etiqueta EM. Etiqueta de parrafo. Etiquetas de encabezado. Etiqueta de titulo. Comentarios en HTML. Mi primera pagina en HTML.'),
(71, 'Estructura de una Pagina Web', 'https://www.youtube.com/embed/OEsTpiwi10E?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Estructura del Lenguaje de HTML. Mi primera pagina en HTML. Formato para crear codigo HTML legible. Etiquetas avanzadas de HTML. Etiquetas para negrita y cursiva. Etiqueta SMALL. Etiquetas para texto/codigo preformateado. Etiquetas pre e ins. Etiqueta de Abreviatura. Etiquetas CITE, ADRESS y BLOCKQUOTE. Etiquetas de subindices y sueprindices.'),
(72, 'Listas en HTML', 'https://www.youtube.com/embed/kRtD8gfM7mI?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Etiquetas de HTML listas.Listas ordenadas en HTML. Listas NO ordenadas en HTML. Listas de definiciones en HTML.'),
(73, 'Tablas en HTML', 'https://www.youtube.com/embed/gENNSJGhn1k?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Etiquetas de tablas en HTML. Etiquetas TABLE, TR, TD. Linea de cabecera en tablas con TH. Etiquetas para agrupar la cabecera, el cuerpo y el pie de una tabla. Etiqueta para el titulo de una tabla. Agrupacion de celdas: por columnas o por filas.'),
(74, 'Enlaces en HTML', 'https://www.youtube.com/embed/htlNk9N0XTA?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Enlaces o Hipertexto. PArtes de un enlace. Tipos de enlaces en HTML. Etiqueta A. Atributo Href.'),
(75, 'Imagenes en HTML', 'https://www.youtube.com/embed/nHHyQE9a5vo?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Imagenes en HTML. Etiqueta IMG. Atributos de la etiqueta IMG. Atributo SRC. Atributo ALT y accesibilidad. Mapas de Imagenes.'),
(76, 'Formularios en HTML (1/2)', 'https://www.youtube.com/embed/3-9NMuohbtg?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Formularios en HTML. Arquitectura Cliente-Servidor. El Navegador como Cliente. Peticiones y Respuestas HTTP. Lenguajes en el cliente y en el Servidor. Comunicacion con Bases de Datos.'),
(77, 'Formularios en HTML (2/2)', 'https://www.youtube.com/embed/-xg-PXD5cTc?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Formularios en HTML. Que es un formulario? Para que sirve un formulario?. Metodos de envio de formularios. Tipos de campos en los formularios. Etiqueta FORM. Etiqueta INPUT. Etiqueta LABEL. Usabilidad. Agrupacion de campos y legendas en formularios.'),
(78, 'Que es el W3C?', 'https://www.youtube.com/embed/eNpHlU6uZ1s?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'W3C. Versiones de HTML. XHTML. HTML5. Etiqueta DOCTYPE. Que es el DOM? API de HTML. Metodos del DOM para Javascript.'),
(79, 'Tecnologias complementarias a HTML', 'https://www.youtube.com/embed/vxiGjd4TXa4?list=PLDcGMW3H0ciAGO8hhnwFIAjrH8VnJmq8p', 'Tecnologias en el lado del cliente. Que es CSS?. Versiones de CSS. Ventajas de usar CSS. Que es Javascript? DHTML. Interactividad entre CSS y Javascript gracias al DOM.'),
(80, 'Creacion de proyecto y comentarios', 'https://www.youtube.com/embed/6PnGXCe9WPI?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Empezamos curso de C#!\r\naprendemos a como crear un proyecto y el uso de los comentarios en c#'),
(81, 'Clases y Objetos', 'https://www.youtube.com/embed/NA-gWKU_cGQ?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Aprende sobre que es una clase y un objeto en c#.'),
(82, 'Creacion de una clase,variables,constructores y metodos', 'https://www.youtube.com/embed/LG1LsAbRSZc?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Aprende a crear una clase en visual c# , ademas de definicion de variables , constructores y metodos.'),
(83, 'Sintaxis de operadores y el if', 'https://www.youtube.com/embed/RYbWHHimKjM?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Aprende la sintaxis de los operadores en c# y la sintaxis del if.'),
(84, 'Vector y Arraylist', 'https://www.youtube.com/embed/1s2wrTWwaTQ?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Explicacion de como definir vectores en c# y el uso del arraylist.'),
(85, 'For y Foreach', 'https://www.youtube.com/embed/3oEsv6p6v-4?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Explicacion de los ciclos For y Foreach.'),
(86, 'Switch', 'https://www.youtube.com/embed/kQQWcgqylJc?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Explicacion y ejemplo del SWITCH'),
(87, 'While', 'https://www.youtube.com/embed/GzC_XI9NFAM?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Explicacion del uso del While en c#'),
(88, 'Do While', 'https://www.youtube.com/embed/XGSpgdWQZB4?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Explicacion del uso del Do While en c#'),
(89, 'Try Catch', 'https://www.youtube.com/embed/dfVoHY7H6Pk?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Como utilizar el Try catch en un proyecto C# para que el programa no se caiga durante la ejecucion.'),
(90, 'List y DataGridView', 'https://www.youtube.com/embed/p4SWe90XNWQ?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Explicacion del uso de la lista generica LIST para acumular objetos completos y del DATAGRIDVIEW para mostrar el contenido de la lista.'),
(91, 'List: Modificar y Eliminar', 'https://www.youtube.com/embed/R9O_RuGCO4U?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Como modificar y borrar informacion en una lista dinamica (LIST)'),
(92, 'Conexion a bases de datos', 'https://www.youtube.com/embed/b_8UNI_eIaw?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Comenzamos con el uso de bases de datos junto a proyectos de C# , en este video les indico una serie de videos sobre la conexion de sql, mysql , access ,LINQ a proyectos de visual studio C# , con lo que podemos comenzar a realizar proyectos ya completos y mas grandes, con este tema termino lo basico del curso y comenzaremos con practicas y temas mas avanzados. '),
(93, 'Manejo de archivos ', 'https://www.youtube.com/embed/1sSYorX0wN4?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Abrir y guardar archivos de texto usando el openfiledialog y el savefiledialog.'),
(94, 'Listbox ', 'https://www.youtube.com/embed/1Ikn_P7HOus?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Anadir, Eliminar y limpiar los campos en un listbox usando C#'),
(95, 'Cargar una imagen en Picturebox ', 'https://www.youtube.com/embed/CQKY6N4PH8M?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Como cargar una imagen en un picturebox desde codigo, usando openfile dialog y la propiedad del picturebox imagelocation.\r\n'),
(96, 'Drag and Drop ', 'https://www.youtube.com/embed/691XfOHiVd4?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Como usar del Drag and Drop (Arrastra y suelta) con diferentes controles del visual studio 2013 en C# (listbox, listview, picturebox, panel...)'),
(97, 'Herencia ', 'https://www.youtube.com/embed/MuFPNFMlThE?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Herencia en C#'),
(98, ' Month Calendar', 'https://www.youtube.com/embed/Q6PdIBUFcCg?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Obteniendo el Valor de un Month Calendar en un textbox en C# '),
(99, 'ProgressBar', 'https://www.youtube.com/embed/o9CioMiXZwk?list=PLD3-wsv2mjgOG9vsb3XkogSD5ZW4x1UAc', 'Aprendiendo a usar el progressbar en c#'),
(100, 'Introduccion e Instalacion del AppServ', 'https://www.youtube.com/embed/sEfcmo-THjM?list=PLF02659CF8418C229', 'Explicacion sobre como instalar appserv'),
(101, 'Editores e Imprimir Datos con Echo y Print', 'https://www.youtube.com/embed/feFxj6xz0hE?list=PLF02659CF8418C229', 'Explicacion de los primeros comandos de php.'),
(102, 'Interaccion con HTML', 'https://www.youtube.com/embed/R6PdB1SktCg?list=PLF02659CF8418C229', 'PHP es un lenguaje que pueda interactuar de manera muy facil con HTML, en este tutorial aprenderas de que manera interactua, y algunos otros conceptos.'),
(103, 'Variables y sus Tipos', 'https://www.youtube.com/embed/vI4-40Nfnis?list=PLF02659CF8418C229', 'En este video tutorial de PHP veremos que son las Variables, como definirlas, como llamarlas y que tipo de variables podemos utilizar en el lenguaje PHP.'),
(104, 'Constantes', 'https://www.youtube.com/embed/lW5I5hcz15s?list=PLF02659CF8418C229', 'En este video tutorial de PHP hablaremos de constantes y la concatenación.'),
(105, 'Arrays o Vectores', 'https://www.youtube.com/embed/e2bD6mhCVc4?list=PLF02659CF8418C229', 'En este video tutorial de PHP hablaremos de Vectores o Arrays.'),
(106, 'Operadores y sus Tipos', 'https://www.youtube.com/embed/aMk4CvOBDNg?list=PLF02659CF8418C229', 'En este video tutorial de PHP hablaremos de los operadores y sus tipos en PHP.'),
(107, 'Condicional If - Else', 'https://www.youtube.com/embed/LqJKVPxqtgk?list=PLF02659CF8418C229', 'En este video tutorial de PHP hablaremos de la sentencia IF ELSE en PHP.'),
(108, 'Bucle While', 'https://www.youtube.com/embed/MDgrsogbiH8?list=PLF02659CF8418C229', 'En este video tutorial de PHP aprenderemos acerca de los bucles, los bucles son estructuras repetitivas que se ejecutan mientras una condicionn sea true, es decir que se cumpla, si esta deja de cumplirse, este deja de reproducirse y ejecutaria el resto del codigo.\r\n'),
(109, 'Bucle Do While', 'https://www.youtube.com/embed/MDgrsogbiH8?list=PLF02659CF8418C229', 'En este video tutorial de PHP aprenderemos acerca del bucle DO while.'),
(110, 'Bucle For', 'https://www.youtube.com/embed/WQzneb7rSMc?list=PLF02659CF8418C229', 'En este video tutorial de PHP aprenderemos acerca del bucle FOR.'),
(111, 'Bucle Foreach', 'https://www.youtube.com/embed/Kubsqlx83rA?list=PLF02659CF8418C229', 'En este video tutorial de PHP aprenderemos acerca del bucle FOR.'),
(112, 'Switch', 'https://www.youtube.com/embed/2hCVmRlnoOM?list=PLF02659CF8418C229', 'En este video tutorial de PHP aprenderemos acerca de la instruccion SWITCH.'),
(113, 'Funciones', 'https://www.youtube.com/embed/zUXDPH3vXu4?list=PLF02659CF8418C229', 'En este video tutorial de PHP veremos que son las funciones que te permiten llamar un bloque de instrucciones que se ejecutara para algunos parametros en especifico, dependiendo de la funcion.'),
(114, 'Formularios', 'https://www.youtube.com/embed/HPjD7GVPeL0?list=PLF02659CF8418C229', 'En este video tutorial de PHP veremos como procesar formularios'),
(115, 'Cómo crear una Calculadora en PHP (1/2)', 'https://www.youtube.com/embed/X52ZPAVtihY?list=PLF02659CF8418C229', ''),
(116, 'Cómo crear una Calculadora en PHP (2/2)', 'https://www.youtube.com/embed/lzTZDjwH1Gs?list=PLF02659CF8418C229', ''),
(117, 'Guardar Archivos de Texto en PHP', 'https://www.youtube.com/embed/MBrXjGDgLrs?list=PLF02659CF8418C229', 'En esta lección aprenderos como guardar archivos de texto en php.'),
(118, 'Leer Archivos de Texto en PHP', 'https://www.youtube.com/embed/wqp1VW6XdUA?list=PLF02659CF8418C229', 'En esta lección aprenderos como leer archivos de texto en php.'),
(119, 'Eliminar Ficheros de Texto en PHP', 'https://www.youtube.com/embed/hnnzl8JWLdc?list=PLF02659CF8418C229', 'En esta lección aprenderos como eliminar archivos de texto en php.'),
(120, 'Insertar datos en MySQL mediante PHP', 'https://www.youtube.com/embed/f0yJDiKdsJc?list=PLF02659CF8418C229', 'En esta lección aprenderos como insertar datos en php.'),
(121, 'Seleccionar Registros de Base de Datos en PHP', 'https://www.youtube.com/embed/F1fMegTpijs?list=PLF02659CF8418C229', 'En esta lección aprenderos como seleccionar registros de nuesrta DB en php.'),
(122, 'Eliminar Registros de Base de Datos en PHP', 'https://www.youtube.com/embed/1lpPGno6nVw?list=PLF02659CF8418C229', 'En esta lección aprenderos como eliminar registros de nuesrta DB en php.'),
(123, 'Sistema de Registro de Usuarios en PHP (Parte 1/2)', 'https://www.youtube.com/embed/CA3YdHcdbwQ?list=PLF02659CF8418C229', 'En esta lección aprenderos como llevar un sistema de registros en php.'),
(124, 'Sistema de Registro de Usuarios en PHP (Parte 2/2)', 'https://www.youtube.com/embed/RJBUoYmMeOQ?list=PLF02659CF8418C229', 'En esta lección aprenderos como llevar un sistema de registros en php.'),
(125, 'Sistema de Ingreso en PHP (Parte 1/2)', 'https://www.youtube.com/embed/MEvL5TyLSXc?list=PLF02659CF8418C229', 'En esta lección aprenderos como llevar un sistema de ingresos en php.'),
(126, 'Sistema de Ingreso en PHP (Parte 2/2)', 'https://www.youtube.com/embed/W4Au5M290Gc?list=PLF02659CF8418C229', 'En esta lección aprenderos como llevar un sistema de ingresos en php.'),
(127, 'Cómo Subir Archivos (Upload) en Php', 'https://www.youtube.com/embed/1AsirYps5Co?list=PLF02659CF8418C229', 'En esta lección aprenderos como subir archivos en php.'),
(128, 'Enviar Correos por PHP', 'https://www.youtube.com/embed/Et6BIoBCUuQ?list=PLF02659CF8418C229', 'En esta lección aprenderos como enviar correos en php.'),
(129, 'Cómo Modificar Registros en una Base de Datos en PHP', 'https://www.youtube.com/embed/jPt8GTu2O7Y?list=PLF02659CF8418C229', 'En esta lección aprenderos como modificar registros de nuestra DB en php.'),
(130, 'Introduccion ', 'https://www.youtube.com/embed/bW-NYf606fM?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Que es sql? y Que son bases de datos?'),
(131, 'Instalar SQL server 2012', 'https://www.youtube.com/embed/udpvZv_C7js?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicación sobre como instalar y configurar sql server.'),
(132, 'CREACION Y ELIMINACION DE UNA BASE DE DATOS', 'https://www.youtube.com/embed/HEkfvk2PBCs?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicación sobre crear y eliminar bases de datos en sql server.'),
(133, 'TIPOS DE DATOS', 'https://www.youtube.com/embed/7gh5MtqkIiU?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicación sobre los tipos de datos con los cuales se trbaja sql server.'),
(134, 'Creacion y eliminacion de tablas', 'https://www.youtube.com/embed/UblULDVT2Fs?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre crear y eliminar tablas  en sql server.'),
(135, 'Insercion de datos (INSERT)', 'https://www.youtube.com/embed/FlkFuNc9psM?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre insertar datos en sql server.'),
(136, 'CONSULTAS', 'https://www.youtube.com/embed/IDljsF3AT2Q?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer consultas de datos en sql server.'),
(137, ' WHERE', 'https://www.youtube.com/embed/W0n64vnpPwc?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como usar WHERE en sql server.'),
(138, 'Operadores Relacionales', 'https://www.youtube.com/embed/d36BhUrhPt4?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer operaciones relacionales en sql server.'),
(139, 'Elimininacion de registros (DELETE, DROP, TRUNCATE)', 'https://www.youtube.com/embed/l9Ijy1g5Doc?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como usar comandos para eliminar datos en sql server.'),
(140, 'Actualizar Registros UPDATE', 'https://www.youtube.com/embed/zqmD6J7X-7E?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como usar comandos para actualizar datos en sql server.'),
(141, 'identity parte 1', 'https://www.youtube.com/embed/7xMtbeLf-DA?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como usar identity en sql server.'),
(142, 'identity parte 2', 'https://www.youtube.com/embed/pLE663Cg5-Y?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como usar identity en sql server.'),
(143, 'operadores aritmeticos', 'https://www.youtube.com/embed/W3LBpUyTJ9Y?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicación sobre como usar operadores arimeticos en sql server.'),
(144, 'Funciones de Agregado', 'https://www.youtube.com/embed/iAcv1jxEuGs?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicación sobre como realizar fundiones de agregado en sql server.'),
(145, 'Concatenacion & Alias', 'https://www.youtube.com/embed/sjZJhj7xSaw?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso de la concatenacion y alias en sql server.'),
(146, 'funciones de manejo de cadenas parte 1', 'https://www.youtube.com/embed/plJ5eZOiBTY?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso de las funciones de manejo con cadenas en sql server.'),
(147, 'Funciones de manejo de cadenas parte 2', 'https://www.youtube.com/embed/_ajYVqI6XzQ?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso de las funciones de manejo con cadenas en sql server.'),
(148, 'Order by', 'https://www.youtube.com/embed/FZe2BT3GFF8?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando order by en sql server.'),
(149, 'Operadores logicos nor and or', 'https://www.youtube.com/embed/brBIaGk5X_A?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando nor en sql server.'),
(150, 'is null - between', 'https://www.youtube.com/embed/Po0d7ediuic?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando between en sql server.'),
(151, ' like not like', 'https://www.youtube.com/embed/B2lVZOuj4ME?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando like en sql server.'),
(152, 'count', 'https://www.youtube.com/embed/fTXiRZV8Imk?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando count en sql server.'),
(153, 'sum - avg', 'https://www.youtube.com/embed/U4538uTXx9w?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando sum en sql server.'),
(154, 'min - max', 'https://www.youtube.com/embed/lSQTzhMzdkQ?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicación sobre como hacer uso del comando min en sql server.'),
(155, 'having', 'https://www.youtube.com/embed/RPiJk40Nubw?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando having en sql server.'),
(156, 'compute', 'https://www.youtube.com/embed/FYfbiUK_anA?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando compute en sql server.'),
(157, 'distinct', 'https://www.youtube.com/embed/nfYShbs1v_M?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando distinct en sql server.'),
(158, 'Top', 'https://www.youtube.com/embed/kcluqgjiSxQ?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer uso del comando top en sql server.'),
(159, 'Respaldo y Restauracion de Bases de Datos', 'https://www.youtube.com/embed/ve6dGwYycHc?list=PL6hPvfzEEMDaU4aiS389oXamdN8sip856', 'Explicacion sobre como hacer respaldos en sql server.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `idUser` int(11) NOT NULL,
  `user` varchar(128) NOT NULL,
  `pass` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`idUser`, `user`, `pass`, `email`) VALUES
(1, 'caryl', 'caryl', 'c.d.m.zepeda@hotmail.com'),
(2, 'user', 'user', 'user');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `leccion`
--
ALTER TABLE `leccion`
  ADD PRIMARY KEY (`idleccion`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`idUser`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
