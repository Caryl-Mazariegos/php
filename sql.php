<!DOCTYPE HTML>

<html>
	<head>
		<title>SQL</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="stylesheet" href="assets/css/main.css" />

	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
			<header id="header">
					<h1 id="logo"><a href="kinal.php">Kinal Academy</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="kinal.php">Inicio</a></li>
							<li>
								<a href="#">Cursos</a>
								<ul>
									<li><a href="java.php">Java</a></li>
									<li><a href="html.php">HTML</a></li>
									<li><a href="php.php">Php</a></li>
									<li><a href="c.php">C#</a></li>
									<li><a href="sql.php">SQL Server</a></li>
									<li><a href="android.php">Android</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</header>
			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>SQL Server</h2>
						</header>
						<div class="row 150%">
							<div class="4u 12u$(medium)">

								<!-- Sidebar -->
									<section id="sidebar">
										<section>
											<h3>Ver lecciones</h3>
											<p>Pincha el boton de abajo para entrar y ver las lecciones que Kinal Academy te ofrece para aprender a programar en SQL</p>
											<footer>
												<ul class="actions">
													<li><a href="sleccion1.php" class="button">Ver Lecciones</a></li>
												</ul>
											</footer>
										</section>
										<hr />
										<section>
											<a href="#" class="image fit"><img src="images/sql.jpg" alt="" /></a>
											<h3>Recursos</h3>
											<p>Haz click en el boton para acceder a la página oficial de microsoft para poder descargar sql server que te ayudará para el gestionamiento de base de datos.
											</p>
											<footer>
												<ul class="actions">
													<li><a href="https://www.microsoft.com/es-es/download/details.aspx?id=42299" class="button">Descargar SQL Server</a></li>
												</ul>
											</footer>
										</section>
									</section>

							</div>
							<div class="8u$ 12u$(medium) important(medium)">

								<!-- Content -->
									<section id="content">
										<a href="#" class="image fit"><img src="images/pic015.jpg" alt="" /></a>
										<h3>Bienvenido al curso de SQL Server</h3>
										<p>s un sistema de manejo de bases de datos del modelo relacional, desarrollado por la empresa Microsoft.

										El lenguaje de desarrollo utilizado (por línea de comandos o mediante la interfaz gráfica de managment studio) es Transact-SQL (TSQL), una implementación del estándar ANSI del lenguaje SQL, utilizado para manipular y recuperar datos (DML), crear tablas y definir relaciones entre ellas (DDL).

										Los competidores principales de SQL Server, en el mercado de las bases de datos relacionales, son productos como Oracle, MariaDB, MySQL, PostgreSQL etc.; SQL Server solo está disponible para sistemas operativos Windows de Microsoft.

										Puede ser configurado para utilizar varias instancias en el mismo servidor físico, la primera instalación lleva generalmente el nombre del servidor, y las siguientes - nombres específicos (con un guión invertido entre el nombre del servidor y el nombre de la instalación). </p>

										<p>El código fuente original de SQL Server que fue utilizado en las versiones previas a la versión 7.0 habría sido comprado de Sybase, pero fue actualizado en las versiones 7.0 y 2000, y reescrito en la versión 2005. Generalmente, cada 2-3 años, una nueva versión es lanzada y, entre estos lanzamientos, se proponen service packes con mejoras y correcciones de bugs, y hotfixes por problemas urgentes en el sistema de seguridad o bugs críticos.</p>

										<h3>¿Qué aprenderé aquí?</h3>

										<p>Kinal academy ha preparado para ti un pensúm corto, pero eficaz para que puedas aprender a manejar bases de datos por medio de SQL Server, las lecciones que Kinal Academy ofrece son:</p>
										<ul>
											<li>Tipos de datos.</li>
											<li>Consultas.</li>
											<li>Actualizar Registros UPDATE.</li>
											<li>Operadores aritmeticos.</li>
											<li>Concatenacion & Alias.</li>
										</ul>
									</section>

							</div>
						</div>
					</div>
				</div>

				<footer id="footer">
					<ul class="copyright">
						<li>&copy; Centro Educativo Técnico Laboral Kinal.</li><li>Caryl Mazariegos</li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>

			<script src="assets/js/main.js"></script>

	</body>
</html>