<?php
session_start();
if (isset($_SESSION['usuario']))
{
?>



<!DOCTYPE HTML>

<html>
	<head>
		<title>Kinal Academy</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />

	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
			<header id="header">
					<h1 id="logo"><a href="kinal.php">Kinal Academy</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="kinal.php">Inicio</a></li>
							<li>
								<a href="#">Cursos</a>
								<ul>
									<li><a href="java.php">Java</a></li>
									<li><a href="html.php">HTML</a></li>
									<li><a href="php.php">Php</a></li>
									<li><a href="c.php">C#</a></li>
									<li><a href="sql.php">SQL Server</a></li>
									<li><a href="android.php">Android</a></li>
								</ul>
							</li>
							<li><p><a href="logout.php">Cerrar sesión</a></p>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<div class="content">
						<header>
							<h2>Kinal Academy</h2>
								<p>Bienvenido: <?php echo $_SESSION['usuario']; ?></p>
							<p>“Primero resuelve el problema. Entonces, escribe el código.”<br />
							— John Johnson.</p>
						</header>
						<span class="image"><img src="images/pic01.jpg" alt="" /></span>
					</div>
					<a href="#one" class="goto-next scrolly">Next</a>
				</section>

			<!-- One -->
				<section id="one" class="spotlight style1 bottom">
					<span class="image fit main"><img src="images/pic02.jpg" alt="" /></span>
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="4u 12u$(medium)">
									<header>
										<h2>Kinal Academy</h2>
									</header>
								</div>
								<div class="4u 12u$(medium)">
									<p>Bienvenido a Kinal Academy, una página en la que podrás encontrar diferentes cursos, vídeos y manuales para distintos lenguajes de programación en los cuales te ayudarán para aprender lo básico en este tema informático.</p>
								</div>
								<div class="4u$ 12u$(medium)">
									<p>Es totalmente gratuito, con solo registrarte puedes tener acceso a todos nuestros cursos.</p>
								</div>
							</div>
						</div>
					</div>
					<a href="#two" class="goto-next scrolly">Next</a>
				</section>

			<!-- Two -->
				<section id="two" class="spotlight style2 right">
					<span class="image fit main"><img src="images/pic03.jpg" alt="" /></span>
					<div class="content">
						<header>
							<h2>Kinal Academy</h2>
							<p>Misión</p>
						</header>
						<p>Nuestra misión como página, es que las personas que ingresen a este portal puedan adentrarse en el mundo de la programación, aprendiendo lo básico en esta página, para ser competencia en el área laboral.</p>
						<p>También es dar a conocer a las personas lo que son los lenguajes de la programación y todo lo que pueden fabricar haciendo uso de estos, de modo que cada vez mas y mas hayan más programadores en nuestro país.</p>
						<ul class="actions">
						</ul>
					</div>
					<a href="#three" class="goto-next scrolly">Next</a>
				</section>

			<!-- Three -->
				<section id="three" class="spotlight style3 left">
					<span class="image fit main bottom"><img src="images/pic04.jpg" alt="" /></span>
					<div class="content">
						<header>
							<h2>Kinal Academy</h2>
							<p>Visión</p>
						</header>
						<p>Ser una página líder en nuestro país, en la cual alcanze un alto número de suscriptores, podiendo así, expandirnos en toda América, siendo la mejor elección para aprendizaje informático.</p>
						<ul class="actions">
						</ul>
					</div>
					<a href="#four" class="goto-next scrolly">Next</a>
				</section>


				<footer id="footer">
					<ul class="copyright">
						<li>&copy; Centro Educativo Técnico Laboral Kinal.</li><li>Caryl Mazariegos</li>
					</ul>
				</footer>

		</div>


			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>

<?php
}
else
{
    echo '<script>location.href = "index.php";</script>'; 
}
?>