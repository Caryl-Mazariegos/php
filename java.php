<!DOCTYPE HTML>

<html>
	<head>
		<title>Java</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="stylesheet" href="assets/css/main.css" />

	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
		<header id="header">
					<h1 id="logo"><a href="kinal.php">Kinal Academy</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="kinal.php">Inicio</a></li>
							<li>
								<a href="#">Cursos</a>
								<ul>
									<li><a href="java.php">Java</a></li>
									<li><a href="html.php">HTML</a></li>
									<li><a href="php.php">Php</a></li>
									<li><a href="c.php">C#</a></li>
									<li><a href="sql.php">SQL Server</a></li>
									<li><a href="android.php">Android</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</header>
			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>Java</h2>
							<p>"write once, run anywhere"</p>
						</header>
						<div class="row 150%">
							<div class="4u 12u$(medium)">

								<!-- Sidebar -->
									<section id="sidebar">
										<section>
											<h3>Ver lecciones</h3>
											<p>Pincha el boton de abajo para entrar y ver las lecciones que Kinal Academy te ofrece para aprender a programar en Java</p>
											<footer>
												<ul class="actions">
													<li><a href="jleccion1.php" class="button">Ver Lecciones</a></li>
												</ul>
											</footer>
										</section>
										<hr />
										<section>
											<a href="#" class="image fit"><img src="images/java.jpg" alt="" /></a>
											<h3>Recursos</h3>
											<p>Haz click en el boton para acceder a la página oficial de oracle para poder descargar los recursos necesarios, recuerda que necesitaras:
											<ul>
												<li>Netbeans</li>
												<li>Java 8</li>
											</ul>
											</p>
											<footer>
												<ul class="actions">
													<li><a href="http://www.oracle.com/technetwork/es/indexes/downloads/index.html" class="button">Ir a Oracle</a></li>
												</ul>
											</footer>
										</section>
									</section>

							</div>
							<div class="8u$ 12u$(medium) important(medium)">

								<!-- Content -->
									<section id="content">
										<a href="#" class="image fit"><img src="images/pic05.jpg" alt="" /></a>
										<h3>Bienvenido al curso de Java</h3>
										<p>Java es un lenguaje de programación de propósito general, concurrente, orientado a objetos que fue diseñado específicamente para tener tan pocas dependencias de implementación como fuera posible. Su intención es permitir que los desarrolladores de aplicaciones escriban el programa una vez y lo ejecuten en cualquier dispositivo (conocido en inglés como WORA, o "write once, run anywhere"), lo que quiere decir que el código que es ejecutado en una plataforma no tiene que ser recompilado para correr en otra. </p>
										<p>El lenguaje de programación Java fue originalmente desarrollado por James Gosling de Sun Microsystems (la cual fue adquirida por la compañía Oracle).</p>
										<h3>¿Qué aprenderé aquí?</h3>
										<p>Kinal academy ha preparado para ti un pensúm corto, pero eficaz para que puedas aprender Java rápidamente y puedas laborar rápidamente, las lecciones que Kinal Academy ofrece son:</p>
										<ul>
											<li>Operaciones con Atributos</li>
											<li>Intercambio Atributos</li>
											<li>Tipo Boolean y Condicionales Anidados</li>
											<li>Tipo Char y Switch</li>
											<li>Bucles While y For</li>
										</ul>
									</section>

							</div>
						</div>
					</div>
				</div>

				<footer id="footer">
					<ul class="copyright">
						<li>&copy; Centro Educativo Técnico Laboral Kinal.</li><li>Caryl Mazariegos</li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>

			<script src="assets/js/main.js"></script>

	</body>
</html>