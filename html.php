<!DOCTYPE HTML>

<html>
	<head>
		<title>HTML5</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />

	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
		<header id="header">
					<h1 id="logo"><a href="kinal.php">Kinal Academy</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="kinal.php">Inicio</a></li>
							<li>
								<a href="#">Cursos</a>
								<ul>
									<li><a href="java.php">Java</a></li>
									<li><a href="html.php">HTML</a></li>
									<li><a href="php.php">Php</a></li>
									<li><a href="c.php">C#</a></li>
									<li><a href="sql.php">SQL Server</a></li>
									<li><a href="android.php">Android</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</header>
			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>HTML5</h2>
							<p>HyperText Markup Language</p>
						</header>
						<div class="row 150%">
							<div class="8u 12u$(medium)">

								<!-- Content -->
									<section id="content">
										<a href="#" class="image fit"><img src="images/pic006.jpg" alt="" /></a>
										<h3>Bienvenido al curso de HTML5</h3>
										<p> Es un estándar que sirve de referencia para la elaboración de páginas web en sus diferentes versiones, define una estructura básica y un código (denominado código HTML) para la definición de contenido de una página web, como texto, imágenes, videos, entre otros. Es un estándar a cargo de la W3C, organización dedicada a la estandarización de casi todas las tecnologías ligadas a la web, sobre todo en lo referente a su escritura e interpretación.</p>
										<p>El lenguaje HTML basa su filosofía de desarrollo en la referenciación. Para añadir un elemento externo a la página (imagen, vídeo, script, entre otros.), este no se incrusta directamente en el código de la página, sino que se hace una referencia a la ubicación de dicho elemento mediante texto. De este modo, la página web contiene sólo texto mientras que recae en el navegador web (interpretador del código) la tarea de unir todos los elementos y visualizar la página final.</p>
										<h3>¿Qué aprenderé aquí?</h3>
										<p>Kinal Academy elaboró un pensúm ideal para ti, para que puedas aprender html5 en poco tiempo, basandose en comandos basicos, sin profundizar en javascript o css, estas son algunas de las lecciones que Kinal Academy te ofrece: </p>
										<ul>
											<li>Etiquetas básicas.</li>
											<li>Estructura de una Página Web.</li>
											<li>Listas en HTML.</li>
											<li>Enlaces en HTML.</li>
											<li>Tecnologías complementarias a HTML.</li>
										</ul>
									</section>

							</div>
							<div class="4u$ 12u$(medium)">

								<!-- Sidebar -->
									<section id="sidebar">
										<section>
											<h3>Ver lecciones</h3>
											<p>Pincha el boton de abajo para entrar y ver las lecciones que Kinal Academy te ofrece para aprender a programar en HTML5</p>
											<footer>
												<ul class="actions">
													<li><a href="hleccion1.php" class="button">Ver Lecciones</a></li>
												</ul>
											</footer>
										</section>
										<hr />
										<section>
											<a href="#" class="image fit"><img src="images/html.jpg" alt="" /></a>
											<h3>Recursos</h3>
											<p>Para programar en html solo necesitaras un editor de texto, Kinal Academy recomienda Notepad++, haz click en el boton para dirigirte a la página oficial de Notepad++ y así puedas descargarlo.</p>
											<footer>
												<ul class="actions">
													<li><a href="https://notepad-plus-plus.org/download/v6.7.9.2.html" class="button">Descargar Notepad</a></li>
												</ul>
											</footer>
										</section>
									</section>

							</div>
						</div>
					</div>
				</div>

				<footer id="footer">
					<ul class="copyright">
						<li>&copy; Centro Educativo Técnico Laboral Kinal.</li><li>Caryl Mazariegos</li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>