<!DOCTYPE HTML>

<html>
	<head>
		<title>C#</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />

	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
		<header id="header">
					<h1 id="logo"><a href="kinal.php">Kinal Academy</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="kinal.php">Inicio</a></li>
							<li>
								<a href="#">Cursos</a>
								<ul>
									<li><a href="java.php">Java</a></li>
									<li><a href="html.php">HTML</a></li>
									<li><a href="php.php">Php</a></li>
									<li><a href="c.php">C#</a></li>
									<li><a href="sql.php">SQL Server</a></li>
									<li><a href="android.php">Android</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>C#</h2>
						</header>
						<div class="row 150%">
							<div class="4u 12u$(medium)">

								<!-- Sidebar -->
									<section id="sidebar">
										<section>
											<h3>Ver lecciones</h3>
											<p>Pincha el boton de abajo para entrar y ver las lecciones que Kinal Academy te ofrece para aprender a programar en c#.</p>
											<footer>
												<ul class="actions">
													<li><a href="cleccion1.php" class="button">Ver Lecciones</a></li>
												</ul>
											</footer>
										</section>
										<hr />
										<section>
											<a href="#" class="image fit"><img src="images/c.jpg" alt="" /></a>
											<h3>Recursos</h3>
											<p>Para poder programar en c# puedes utilizar microsoft visual studio, haz click en el boton para dirigirte a la página oficial y poder descargar el IDE.</p>
											<footer>
												<ul class="actions">
													<li><a href="https://www.visualstudio.com/es-es/products/visual-studio-express-vs.aspx" class="button">Descargar VisualStudio</a></li>
												</ul>
											</footer>
										</section>
									</section>
							</div>
							<div class="8u$ 12u$(medium) important(medium)">

								<!-- Content -->
									<section id="content">
										<a href="#" class="image fit"><img src="images/pic009.jpg" alt="" /></a>
										<h3>Bienvenido al curso de C#</h3>
										<p>La plataforma .NET de Microsoft es un componente de software que puede ser añadido al sistema operativo Windows. Provee un extenso conjunto de soluciones predefinidas para necesidades generales de la programación de aplicaciones, y administra la ejecución de los programas escritos específicamente con la plataforma. Esta solución es el producto principal en la oferta de Microsoft, y pretende ser utilizada por la mayoría de las aplicaciones creadas para la plataforma Windows.
.NET Framework se incluye en Windows Server 2008, Windows Vista y Windows 7. De igual manera, la versión actual de dicho componente puede ser instalada en Windows XP, y en la familia de sistemas operativos Windows Server 2003. Una versión "reducida" de .NET Framework está disponible para la plataforma Windows Mobile, incluyendo teléfonos inteligentes.</p>
										<p>Durante el desarrollo de la plataforma .NET, las bibliotecas de clases fueron escritas originalmente usando un sistema de código gestionado llamado Simple Managed C (SMC). En enero de 1999, Anders Hejlsberg formó un equipo con la misión de desarrollar un nuevo lenguaje de programación llamado Cool (Lenguaje C orientado a objetos). Este nombre tuvo que ser cambiado debido a problemas de marca, pasando a llamarse C#.3 La biblioteca de clases de la plataforma .NET fue migrada entonces al nuevo lenguaje.</p>
										<h3>¿Qué aprenderé aquí?</h3>
										<p>Kinal Academy elaboró este pensúm para que puedas programar de forma rápida y eficaz c#, estas son las lecciones que c# te ofrece: .</p>
										<ul>
											<li>Clases y Objetos.</li>
											<li>Vector y Arraylist.</li>
											<li>List y DataGridView.</li>
											<li>Conexion a bases de datos .</li>
											<li>Drag and Drop .</li>
										</ul>
									</section>

							</div>
						</div>
					</div>
				</div>
				
				<footer id="footer">
					<ul class="copyright">
						<li>&copy; Centro Educativo Técnico Laboral Kinal.</li><li>Caryl Mazariegos</li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>