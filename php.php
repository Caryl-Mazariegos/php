<!DOCTYPE HTML>

<html>
	<head>
		<title>PHP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="stylesheet" href="assets/css/main.css" />

	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
		<header id="header">
					<h1 id="logo"><a href="kinal.php">Kinal Academy</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="kinal.php">Inicio</a></li>
							<li>
								<a href="#">Cursos</a>
								<ul>
									<li><a href="java.php">Java</a></li>
									<li><a href="html.php">HTML</a></li>
									<li><a href="php.php">Php</a></li>
									<li><a href="c.php">C#</a></li>
									<li><a href="sql.php">SQL Server</a></li>
									<li><a href="android.php">Android</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</header>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>PHP</h2>
							<p>Hypertext Pre-processor</p>
						</header>

						<div class="row 150%">
							<div class="8u 12u$(medium)">

								<!-- Content -->
									<section id="content">
										<a href="#" class="image fit"><img src="images/pic008.jpg" alt="" /></a>
										<h3>Bienvenido al curso de Php</h3>
										<p>Es un lenguaje de programación de uso general de código del lado del servidor originalmente diseñado para el desarrollo web de contenido dinámico. Fue uno de los primeros lenguajes de programación del lado del servidor que se podían incorporar directamente en el documento HTML en lugar de llamar a un archivo externo que procese los datos. El código es interpretado por un servidor web con un módulo de procesador de PHP que genera la página Web resultante. </p>
										<p>PHP se considera uno de los lenguajes más flexibles, potentes y de alto rendimiento conocidos hasta el día de hoy, lo que ha atraído el interés de múltiples sitios con gran demanda de tráfico, como Facebook, para optar por el mismo como tecnología de servidor.</p>
										<h3>¿Qué aprenderé aquí?</h3>
										<p>En este curso aprenderás lo básico para poder programar en Php. Aprenderás a iniciar tu propio servidor local para poder empezar a trabajar en este lenguaje, estas son algunas de las lecciones que Kinal Academy te ofrece: </p>
										<ul>
											<li>Interacción con HTML.</li>
											<li>Formularios.</li>
											<li>Guardar Archivos de Texto en PHP.</li>
											<li>Condicional If - Else.</li>
											<li>Funciones.</li>
										</ul>
									</section>

							</div>
							<div class="4u$ 12u$(medium)">

								<!-- Sidebar -->
											<section id="sidebar">
										<section>
											<h3>Ver lecciones</h3>
											<p>Pincha el boton de abajo para entrar y ver las lecciones que Kinal Academy te ofrece para aprender a programar en php</p>
											<footer>
												<ul class="actions">
													<li><a href="pleccion1.php" class="button">Ver Lecciones</a></li>
												</ul>
											</footer>
										</section>
										<hr />
										<section>
											<a href="#" class="image fit"><img src="images/php.jpg" alt="" /></a>
											<h3>Recursos</h3>
											<p>Para poder programar en php necesitarás un programa que pueda levantar un servidor, entre ellos puedes enconrtar xampp o wamp, en el boton de abajo te dejamos el link para descargar xampp.</p>
											<footer>
												<ul class="actions">
													<li><a href="https://www.apachefriends.org/es/download.html" class="button">Descargar Xampp</a></li>
												</ul>
											</footer>
										</section>
									</section>

							</div>
						</div>
					</div>
				</div>

				<footer id="footer">
					<ul class="copyright">
						<li>&copy; Centro Educativo Técnico Laboral Kinal.</li><li>Caryl Mazariegos</li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>